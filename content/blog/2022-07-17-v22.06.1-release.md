title: "v22.06 SP1: The One Where We Added The Pro & E7"
title-short: "v22.06 SP1"
date: 2022-07-17
preview: "2022-07/ppp-stable_thumb.jpg"
---

The first service pack for the v22.06 release is here! It adds two new devices to the stable release that did not make
it into the original release of v22.06. It also has a few nice stability improvements and adds
the new Megapixels and postprocessd changes for improved picture quality on the PinePhone.

[#grid side#]
[![](/static/img/2022-07/ppp-stable_thumb.jpg){: class="w300 border"}](/static/img/2022-07/ppp-stable.jpg)
<br><span class="w300">
A photo of the PPP running postmarketOS v22.06. Shot with Megapixels and
postprocessd.
</span>
[#grid text#]

## Contents

* [PINE64 PinePhone Pro](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3280) has been added, with support
  for the postmarketOS installer. Just like with the OG PinePhone, the installer allows using encryption and installing
  from SD to eMMC. Find installation instructions in the [updated wiki page](https://wiki.postmarketos.org/wiki/PINE64_PinePhone_Pro_(pine64-pinephonepro)) as always.

* [Samsung Galaxy E7](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3188) has been backported
  to the stable release too. Again, reading the device's [wiki page](https://wiki.postmarketos.org/wiki/Samsung_Galaxy_E7_(samsung-e7)) is a good idea.

* [Modem udev rules](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3235)
  have been added to improve the stability when using the Biktorgj firmware on the PinePhone modem

* [Megapixels 1.5.2](https://gitlab.com/postmarketOS/megapixels/-/tags/1.5.2)
  is now available together with the updated version of postprocessd for
  improved picture quality and faster postprocessing on the supported
  Megapixels devices. To make use of it, run "`apk add postprocessd`" in existing
  installations. New installation images will have it installed by default.
  See the video *Megapixels and postprocessd improvements*
  ([PeerTube](https://spacepub.space/w/56a99cf0-0428-45b6-a11e-ba7ce0173b7c),
   [YouTube](https://www.youtube.com/watch?v=Ypc3pfzSajo)) for details.

[#grid end#]

* [osk-sdl 0.67](https://gitlab.com/postmarketOS/osk-sdl/-/tags/0.67) powers
  the device off when pressing the power button (in case you don't feel like
  unlocking your phone on boot right now), and fixes the issues with slow input
  when using an external keyboard. <b>UPDATE 2022-07-20:</b> We've gotten two
  reports that input is broken with this osk-sdl version on the PinePhone;
  although we can't reproduce it yet, the osk-sdl update has been reverted.
  Details and recovery instructions in
  [pma#1618](https://gitlab.com/postmarketOS/pmaports/-/issues/1618).


## How to get it

Find the most recent images at our [download page](/download/). Existing users of the v22.06 release will receive this
service pack automatically on their next system update. If you read this blog post right after it was published, it may
take a bit until binary packages and new images are available.

Thanks to everybody who made this possible, especially our amazing community members and upstream projects.
