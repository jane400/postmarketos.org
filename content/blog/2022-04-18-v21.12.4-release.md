title: "v21.12 Service Pack 4"
title-short: "v21.12 SP4"
date: 2022-04-18
preview: "2022-04/pinephone_easter.jpg"
---


[![](/static/img/2022-04/pinephone_easter.jpg){: class="wfull border"}](/static/img/2022-04/pinephone_easter.jpg)

Look what the easter bunny dragged in, the fourth service pack for the v21.12 stable release. This time we got:

## Contents

* [Samsung Galaxy Tab 2 7.0"](https://wiki.postmarketos.org/wiki/Samsung_Galaxy_Tab_2_7.0%22_(samsung-espresso3g)) has
  been backported. This device already was in the
  [community category](https://wiki.postmarketos.org/wiki/Device_categorization) when v21.12 was branched from edge
  ([!2561](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2651)). However it wasn't included in the release
  yet as it didn't work without proprietary PowerVR GPU drivers (and would have needed hacks like a forked wlroots...).
  But now it's all figured out, the hackish GPU acceleration was dropped in favor of using
  [proper mainline](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3027).

* [ODROID HC2](https://wiki.postmarketos.org/wiki/ODROID_HC2_(odroid-hc2)) kernel upgrade to
  [5.16.14](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3006). Now running on mainline with zero patches.

* [Nokia N900](https://wiki.postmarketos.org/wiki/Nokia_N900_(nokia-n900)) Linux kernel upgrade to
  [5.15.31](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3032).

* [SDM845](https://wiki.postmarketos.org/wiki/Qualcomm_Snapdragon_845_(SDM845)) Linux kernel upgrade to
  [5.17.0](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3031), bringing call audio support to the
  Xiaomi PocoPhone F1 ([!2982](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2982)).

* [PINE64 PinePhone](https://wiki.postmarketos.org/wiki/PINE64_PinePhone_(pine64-pinephone)):
  set `lima.sched_timeout_ms=2000` to work around UI freezing under heavy load, with endless flickering of the last two
  frames ([#805](https://gitlab.com/postmarketOS/pmaports/-/issues/805)).
  While this is a great usability improvement (who likes to have their screen lock up, right?), it's masking whatever
  problems cause the rendering to take up to two full seconds to draw a frame. Therefore we only applied it on stable
  and not on edge, where we would rather find a proper fix. Help with figuring this out is appreciated, more
  information can be found in the issue.

* [postmarketOS Tweaks 0.11](https://gitlab.com/postmarketOS/postmarketos-tweaks/-/tags/0.11.0) fixes the custom audio
  profile settings and makes the app look a bit more modern.

* [Powersupply 0.6](https://gitlab.com/MartijnBraam/powersupply/-/tags/0.6.0) shows battery information from the
  PinePhone keyboard case.

## How to get it

Find the most recent images at our [download page](/download/). Existing users of the v21.12 release will receive this
service pack automatically on their next system update. If you read this blog post right after it was published, it may
take a bit until binary packages are available.

Thanks to everybody who made this possible, especially our amazing community members and upstream projects.
