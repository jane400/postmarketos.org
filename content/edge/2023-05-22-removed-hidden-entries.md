title: "postmarketos-hidden-desktop-entries removed"
date: 2023-05-22
---

The package postmarketos-hidden-desktop-entries has been removed. If you still
have it installed, run:

```
# apk del postmarketos-hidden-desktop-entries
```

postmarketos-hidden-desktop-entries was introduced to improve the user
experience in the cases where some applications appeared as available, but could
not be lauched graphically. Since its introduction, all the problems that
prevented the apps to be launched have been fixed, making the package
unnecessary.

Related: [pmaports!4086](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4086)
