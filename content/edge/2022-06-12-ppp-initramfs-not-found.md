title: "PinePhone Pro: initramfs-extra not found on boot"
date: 2022-06-12
---
A recent change to the `postmarketos-mkinitfs` package in `1.4.1-r5` causes the
initramfs to be built without support for the ascii NLS in the kernel, which is
required for the fat32 file system used in EFI boot. Since the Pinephone Pro
uses EFI boot now, this broke booting on this platform.


```
...
EFI stub: ERROR: FIRMWARE BUG: kernel image not aligned on 64k boundary
...
Mount boot partition (/dev/mmcblk0p1) to /boot (read-only)
Detected vfat filesystem
[    3.680320] FAT-fs (mmcblk0p1): IO charset ascii not found
mount: mounting /dev/mmcblk0p1 on /boot failed: Invalid argument
ERROR: initramfs-extra not found!
```

This will be fixed in `postmarketos-mkinitfs-1.4.1-r7`. In the meantime, if you
have already upgraded to `postmarketos-mkinitfs-1.4.1-r5` or `-r6`, do not
reboot until the `1.4.1-r7` is available/installed on your device.

If you have already rebooted, you can recover the system by booting the
Pinephone Pro into jump drive, mounting the `/boot` partition on your host
system, and copying [the `initramfs` file from
here](https://nc.postmarketos.org/index.php/s/ZqkSHtK2dHkBJfF/download/initramfs)
to the partition, overwriting the existing `initramfs` file.

The sha256 checksum for this initramfs file is:

[initramfs](https://nc.postmarketos.org/index.php/s/ZqkSHtK2dHkBJfF/download/initramfs) `6bde0e6fdc383829e0c94cc46ce2b93d17c11214cfc6f301ac6d6d686eceed04`

### Also see

- Fix @ [pma!3229](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3229)
- Issue @ [pma!1555](https://gitlab.com/postmarketOS/pmaports/-/issues/1555)
