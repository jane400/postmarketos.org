# Copyright 2022 Oliver Smith, Martijn Braam
# SPDX-License-Identifier: AGPL-3.0-or-later
import flask_frozen
import glob
import os

# current dir
import app
import config
import config.wiki
import page


freezer = flask_frozen.Freezer(app.app)
app.app.config['FREEZER_DESTINATION'] = 'docs'
app.app.config['FREEZER_BASE_URL'] = 'https://postmarketos.org/'


@freezer.register_generator
def blog_post():
    for f in os.listdir(config.content_dir_blog):
        y, m, d, *title = f[:-3].split('-')
        slug = '-'.join(title)
        yield { 'y': y, 'm': m, 'd': d, 'slug': slug }

@freezer.register_generator
def static_page_redirect():
    for f in os.listdir(config.content_dir_page):
        page = f[:-3]
        yield { 'page': page }

@freezer.register_generator
def static_page_or_wiki_redirect():
    for f in os.listdir(config.content_dir_page):
        slug = f[:-3]
        yield {'slug': slug}
    for slug, redirect in config.wiki.redirects.items():
        yield {'slug': slug}

    # Avoid MissingURLGeneratorWarning
    yield 'static', {'filename': ''}


def check_conflicts():
    slugs = ["blog", "edge", "static"]

    for f in os.listdir(config.content_dir_page):
        slug = f[:-3]
        if slug in slugs:
            raise RuntimeError(f"Page {slug}.md generates conflicting URL!")
        slugs.append(slug)

    for slug, redirect in config.wiki.redirects.items():
        if slug in slugs:
            raise RuntimeError(f"config.wiki.redirects: '{slug}' generates"
                                " conflicting URL!")


def check_preview_images():
    for post in glob.glob(f"{config.srcdir}/content/*/*.md"):
        data, content = page.parse_yaml_md(post)
        if "preview" not in data:
            continue
        preview_path = f"{config.srcdir}/static/img/{data['preview']}"
        if not os.path.exists(preview_path):
            err_name = "/".join(post.split("/")[-3:])
            raise RuntimeError(f"{err_name}: preview image doesn't exist:"
                               f" {preview_path}")


if __name__ == '__main__':
    print("Checking for conflicting URLs...")
    check_conflicts()
    print("Checking paths to preview images...")
    check_preview_images()
    print("Freezing the website to 'docs'...")
    freezer.freeze()
