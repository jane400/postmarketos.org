# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later
import html
import os
import yaml

import config.download
import config.mirrors

def grid(html):
    """ Replace the following markers with appropriate <div class="..."> and
        </div> tags. See README.md and static/code/blog-post.css for more
        information.
        - "[#grid side#]"
        - "[#grid text#]"
        - "[#grid bottom#]"
        - "[#grid end#]"
        :param html: blog post code (already converted from markdown to HTML)
        :returns: html with all markers replaced """
    sections = ["side", "text", "bottom"]
    ret = ""
    in_grid = False

    for word in html.split("[#grid "):
        # Continue or start grid
        if in_grid:
            # Avoid "<p></div>"
            if ret[-3:] == "<p>":
                ret = ret[:-3]
            ret += "</div>"

        # New grid section
        tag_found = ''
        for section in sections:
            tag = section + "#]"
            if word.startswith(tag):
                tag_found = tag
                if not in_grid:
                    ret += '<div class="grid">'
                    in_grid = True
                ret += '<div class="grid-' + section + '">'
                break

        # End grid
        tag = "end#]"
        if word.startswith(tag):
            if not in_grid:
                raise ValueError("[#grid end#] found before it was opened!")
            tag_found = tag
            ret += "</div>"
            in_grid = False

        # Remove tag from word
        word = word[len(tag_found):]

        # Avoid "<div class=...></p>"
        if word[:4] == "</p>":
            word = word[4:]

        ret += word

    # Check for grids without end tag
    if in_grid:
        raise ValueError("Missing [#grid end#]!")
    return ret


def download_table(html):
    marker = "[#download table#]"
    if marker not in html:
        return html

    imgs_url = config.download.imgs_url
    latest_release = config.download.latest_release

    new = "<table class='table-specs'>\n"
    new += "<td><b>Main</b></td><td><b>Stable</b></td><td><b>Edge</b></td>"

    for category, category_cfg in config.download.table.items():
        if category != "Main":
            new += f"<tr><td colspan='2'><b>{category}</b></td></tr>\n"
        for device, device_cfg in category_cfg.items():
            new += "<tr>"
            name = device_cfg["name"]
            link = f"{imgs_url}/{latest_release}/{device}/"
            link_edge = f"{imgs_url}/edge/{device}/"
            new += f"<td class='device-name'>{name}</td>\n"
            new += f"<td><a href='{link}' class='download-link' " \
                    "style='text-decoration-color: #009900bf;'>" \
                    f"{latest_release}</a></td>\n"
            new +=  "<td style='white-space: nowrap;'> "\
                   f"<a href='{link_edge}' class='download-link' " \
                    "style='text-decoration-color: orange;'>edge " \
                    "</a> 🚧</td></tr>\n"

    new += "</table>\n"
    return html.replace(marker, new)


def mirrors_list(html_str):
    marker = "[#mirrors list#]"
    if marker not in html_str:
        return html_str

    new = ""
    for name, mirror_cfg in config.mirrors.mirrors.items():
        urls_html = ""
        for url in mirror_cfg["urls"]:
            protocol_esc = html.escape(url.split(":", 1)[0])
            url_esc = html.escape(url)
            urls_html += f"<a href='{url_esc}'>{protocol_esc}</a> "

        location = html.escape(mirror_cfg.get("location", "Unknown Location"))

        new += f"<b>{html.escape(name)}</b><br>\n"
        new += f"{location}\n"
        if "bandwidth" in mirror_cfg:
            new += f", {html.escape(mirror_cfg['bandwidth'])}"
        new += f"<br>\n"
        new += f"{urls_html}\n"
        new += "<br><br>\n"

    return html_str.replace(marker, new)


def replace(html):
    """ Various replacements for blog posts, to make them responsive etc.
        :param html: blog post code (already converted from markdown to HTML)
        :returns: html with replacements made """
    ret = html
    ret = grid(ret)
    ret = download_table(ret)
    ret = mirrors_list(ret)

    ret = ret.replace("[#latest release#]",
                      config.download.latest_release)
    return ret


def parse_yaml_md(path):
    """ Load one of the blog post / edge post / page .md files.
        :returns: data, content
                  * data: the parsed yaml frontmatter
                  * content: markdown code below "---"
    """
    with open(os.path.join(path), encoding="utf-8") as handle:
        raw = handle.read()
    frontmatter, content = config.regex_split_frontmatter.split(raw, 2)

    data = yaml.load(frontmatter, Loader=yaml.Loader)
    return data, content
